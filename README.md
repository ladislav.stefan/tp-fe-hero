# Frontend task

Make a small React application connected to open GraphQL API.
Create a page with a sortable list of train stations in/near Munich.
Make a simple search dialog and filter results. 

## Requirements
 - React application connected to API (Apollo)
 - Typescript
 - Basic UI (we prefer MaterialUI)
 - Tests
 
## How to do it:
 - clone this repository
 - make a merge request for the review

### Open API details:
 - server end-point (CORS origin: *, mocked only nearby.stations with given lat, long): https://tapi-iota.vercel.app/api/gql
 - GraphQL schema and playground: http://bahnql.herokuapp.com/graphql
 - query: nearby.stations
 - hint: Munich latitude: 48.1390056N, longitude: 11.5577837E
